 <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2018-<?php echo date('Y'); ?> Watch Hax | Version 1.1 </p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="https://www.watchhax.me/">Support</a></li>
        </ul>
      </footer>