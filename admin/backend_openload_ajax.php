<?php
/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
require_once 'validate.php';
	//require '../name.php';
	//require 'header.php';
	//require 'nav_bar.php';
 require_once '../st-config.php';

// Check connection
if($conn === false){
    die("ERROR: Could not connect. " . $conn->connect_error);
}

// Escape user inputs for security
$term = $conn->real_escape_string($_REQUEST['term']);

if(isset($term)){
    // Attempt select query execution
    $sql = "SELECT * FROM room WHERE title LIKE '%" . $term . "%' LIMIT 5";
    if($result = $conn->query($sql)){
        if($result->num_rows > 0){
            $results = [];
            while($row = $result->fetch_array()){
              $row = (object) $row;
                // echo '<p style="color:transparent;">'.$row['room_id'].'<input style="background-color:skyblue;" type="text" placeholder="'.$final_result.'"></p>';
                $results[]   = ['id' => $row->room_id, 'text' => $row->title, 'image' => $row->image];


            }
            $results = json_encode($results);
            echo $results;
            // Free result set
            // $result->free();
        } else{
            echo "<p>No matches found</p>";
        }
    } else{
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
}
// function findProfessional(Request $request)
// {
//
//
//   $term = trim($request->q);
//
//        if (empty($term)) {
//            return \Response::json([]);
//        }
//
//        $professionals = $this->professional->find($term);
//
//        $formatted_professionals = [];
//
//        foreach ($professionals as $professionalAddress) {
//        }
//
//        return \Response::json($formatted_professionals);
//    }
// Close connection
$conn->close();
?>
