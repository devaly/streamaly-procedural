<?php
	require_once 'validate.php';

	require 'header.php';

?>

  <body>
    <?php include 'nav_bar.php'; ?>

    <br />
    <div class="container-fluid">

      <div class="panel panel-default">
        <div class="panel-body">
          <table id="table" class="table table-hover">
						<a style="float:right;margin-right:50px;" href="newanime.php" class="btn btn-success " >Add New</a>
            <thead>
              <tr class="table-active">

                <th scope="col">File Name </th>
                <th scope="col">Slug </th>

                <th scope="col">Tmdb Number</th>
                <th scope="col">Imdb</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
						$query = $conn->query("SELECT * FROM `animeStreaming` ") or die(mysqli_error());
						while($fetch = $query->fetch_array()){

					?>
                <tr>
                  <td> <?php echo $fetch['name'].'<strong> Season '.$fetch['gotype']."</strong>"; ?></td>
                  <td><strong> <?php echo $fetch['slug']; ?></strong></td>
                  <td><strong> <?php echo $fetch['tmdb']; ?></strong></td>
                  <td>
                      <?php echo $fetch['imdb']; ?>

                    </td>

                    <td><a class="btn btn-warning" href="newanime.php?id=<?php echo $fetch['id']; ?>"><i class = "glyphicon glyphicon-edit"></i> Edit</a></td>

                </tr>
                <?php
						}
					?>
            </tbody>
          </table>



        </div>
      </div>
    </div>
    <br />
    <br />
    </div>
    <?php include'footer.php'; ?>

  </body>

  <script type="text/javascript">
    function confirmationDelete(anchor) {
      var conf = confirm("Are you sure you want to delete this record?");
      if (conf) {
        window.location = anchor.attr("href");
      }
    }
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      $("#table").DataTable();
    });
  </script>

  </html>
