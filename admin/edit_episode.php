<?php
	require_once 'validate.php';
 require_once('../st-config.php');

		require_once('header.php');
		require_once('nav_bar.php');


					$query = $conn->query("SELECT * FROM `room` WHERE `room_id` = '$_REQUEST[imdb_id]'") or die(mysqli_error());
						$fetch = $query->fetch_array();

					?>

  <body class="bg-light">

    <div class="container">
      
<br>
      <Br>
        <br>
      <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span style="display:none;" class="text-muted">You Are Editing</span>
          </h4>
          <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="<?php echo $fetch['image']; ?>" alt="" width="250" height="300">
        <h2><?php echo $fetch['title']; ?></h2>
        
      </div>
        </div>
        <div class="col-md-8 order-md-1">
          <form class="needs-validation" action="edit_query_episode.php" method="POST">
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Season</label>
                 <input type="hidden" class="form-control" id="firstName" placeholder="" name="imdb_id" value="<?php echo $fetch['room_id']; ?>" >

                <input type="text" class="form-control" id="firstName" placeholder="" name="season_number" value="<?php echo $fetch['season']; ?>" >
                
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Episode</label>
                <input type="text" class="form-control" id="lastName" placeholder="" name="episode_number" value="<?php echo $fetch['episode'];?>" >
                
              </div>
            </div>

            <div class="mb-3">
              <label for="username">Openload Embed</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">URL</span>
                </div>
                <input type="text" class="form-control" id="username" name="oload_embed_link" value="<?php echo $fetch['openload']; ?>" placeholder="Openload Embed" required>
             
              </div>
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                
                <label for="zip">TMDB</label>
                <input type="text" class="form-control" name="tmdb" value="<?php echo $fetch['TMDB']; ?>" id="zip" placeholder="" >
                
              </div>
            </div>            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" name="update_tv_show" type="submit">Update</button>
          </form>
        </div>
      </div>
<?php include'footer.php'; ?>
     
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
  </body>
</html>
