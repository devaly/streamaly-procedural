<?php require_once "../st-config.php"?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="https://cdn3.iconfinder.com/data/icons/iconic-1/32/play_alt-512.png">

    <title>Login | <?php echo site_title; ?></title>

    <!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://bootswatch.com/4/flatly/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form method="POST"  class="form-signin">
      <img class="mb-4" src="https://cdn3.iconfinder.com/data/icons/iconic-1/32/play_alt-512.png" alt="" width="72" height="72">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
      <label for="inputEmail" class="sr-only">Username</label>
      <input type="text" id="inputEmail" name = "username" class="form-control" placeholder="Username" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" name = "password" class="form-control" placeholder="Password" required>
      
      <button name="login" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <br>
	<?php require_once 'login.php'?>
    <?php include'footer.php'; ?>  
  </form>
    				

  </body>
</html>