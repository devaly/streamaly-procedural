<?php
/* Attempt MySQL server connection. Assuming you are running MySQL
server with default setting (user 'root' with no password) */
require_once 'validate.php';
	//require '../name.php';
	//require 'header.php';
	//require 'nav_bar.php';
 require_once '../st-config.php';

// Check connection
if($conn === false){
    die("ERROR: Could not connect. " . $conn->connect_error);
}
 
// Escape user inputs for security
$term = $conn->real_escape_string($_REQUEST['s']);
 ?>

				<table id = "table" class="table table-hover">
					<thead>
						<tr class="table-active">

							<th scope="col">Title</th>

							<th scope="col">Update Links</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
<?php
if(isset($term)){
    // Attempt select query execution
    $sql = "SELECT * FROM room WHERE CONCAT_WS( title, room_id, TMDB) LIKE '%" . $term . "%'  LIMIT 20";
    if($result = $conn->query($sql)){
        if($result->num_rows > 0){            
            while($fetch = $result->fetch_array()){
              $final_result = str_replace(" ", "",$fetch['title']);?>
          
              
              <tr>
														<td>
                              <?php 
              $ftitle = $fetch['title'] ;
              if(!empty($ftitle)){
                echo $fetch['title'];
                
              }
              else{
                
                echo "error";
              }
              ?><strong>        <?php echo '<a target="_blank"href="https://streamaly.me/player.php?play='.$fetch['room_id'].'">'.$fetch['room_id'].'</a>';?></strong>

						<td>
								<input type="hidden" value="server<?php echo $_SESSION['room_update']; ?>" name="room_update">
								<a class = "btn" href="imdb2/openload.php?action=update&imdbid=<?php echo $fetch['room_id'];?>">Update</a>
              <button class = "btn btn-primary" <?php if(!empty($fetch['e_type']) AND $fetch['e_type'] === "movie")echo "disabled";  ?>>
                <a  style="<?php if(!empty($fetch['e_type']) AND $fetch['e_type'] === "movie")echo "display:none;";  ?>" href="imdb2/openload.php?action=add_new_episode&g=1&imdbid=<?php echo $fetch['room_id'];?>&type=<?php
              $e_type = $fetch['e_type'];
              if(!empty($e_type)){
              echo $fetch['e_type'];}
                  else if(empty($e_type)){
                    $fetch['type'];                    
                  }                                                                                                             
                                                                                                                               ?>">Add New</a>
              <?php if(!empty($fetch['e_type']) AND $fetch['e_type'] === "movie")echo "Disabled";  ?>
              </button>
                </td>

							<td><center><a class = "btn btn-warning" href = "edit_episode.php?action=edit&imdb_id=<?php
							echo $fetch['room_id']?>"><i class = "glyphicon glyphicon-edit"></i> Edit</a> <a class = "btn btn-danger" onclick = "confirmationDelete(this); return false;" href = "delete_episode.php?room_id=<?php echo $fetch['room_id']?>"><i class = "glyphicon glyphicon-remove"></i> Delete</a></center></td>

						</tr>
				
					
              <?php
              
            }?>
	</tbody>
				</table>
		
			</div>
		</div>
	</div>
	<br />
	<br />
	<div style = "text-align:right; margin-right:10px;" class = "navbar navbar-default navbar-fixed-bottom">
	</div>

<?php
            // Free result set
            $result->free();
        } else{
            echo "<p>No matches found</p>";
        }
    } else{
        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
    }
}
 
// Close connection
$conn->close();
?>