<?php
/*
* -------------------------------------------------------------------------------------
* @author: Doothemes
* @author URI: https://doothemes.com/
* @aopyright: (c) 2017 Doothemes. All rights reserved
* -------------------------------------------------------------------------------------
*
* @since 2.1.3
* Edited by https://www.watchhax.me
*/

$dt_player	= get_post_meta($post->ID, 'repeatable_fields', true);
$reports	= get_post_meta($post->ID, 'numreport', true);
$views		= dt_get_meta('dt_views_count');
$light		= get_option('dt_player_luces','true');
$report		= get_option('dt_player_report','true');
$ads		= get_option('dt_player_ads','not');
$qual		= get_option('dt_player_quality','true');
$viewsc		= get_option('dt_player_views','true');
$clic		= get_option('dt_player_ads_hide_clic','true');
$time		= get_option('dt_player_ads_time','20');
$ads_300	= get_option('dt_player_ads_300');
$page	    = get_option('dt_jwplayer_page');
$gdrivepage = get_option('dt_jwplayer_page_gdrive');
// Player
$ids = dt_get_meta('ids');

$tmdb = dt_get_meta('ids');
//$ids = $id_amino;
if (preg_match('/tt/',$ids)) {
$ids = dt_get_meta('ids');
}else {
$jsons_aka = file_get_contents('http://api.themoviedb.org/3/tv/'.$ids.'?api_key=05902896074695709d7763505bb88b4d&append_to_response=external_ids');
$uncompressed = json_decode($jsons_aka);
$ids= $uncompressed->external_ids->imdb_id;
$idsanime= $uncompressed->external_ids->imdb_id;
$ids_tmdb= $uncompressed->external_ids->id;

$temp = dt_get_meta('temporada');
$epis = dt_get_meta('episodio');
$ids = $ids.'-'.$temp.'-'.$epis;
}
?>
<style>
.blacked {
background-color:black;

}




#loader1 {
    position:absolute;
    left:40%;
    top:35%;
}
</style>

<?php
if(!empty($temp)){
  $change_me = "tv";


}
else if(empty($temp)){
  $change_me2 = "movie";

}

$jsons_aka = file_get_contents('https://api.themoviedb.org/3/'.$change_me.$change_me2.'/'.$tmdb.'?api_key=05902896074695709d7763505bb88b4d&language=en-US');
$uncompressed = json_decode($jsons_aka);
$backdrop_path= $uncompressed->backdrop_path;


?>
 <style>
      .ads_player {
    position:absolute;
    top:0;
    left:0;
    z-index:2;
    width:100%;
    height:100%;
    background-image: url("https://image.tmdb.org/t/p/original<?php echo $backdrop_path; ?> ");
        background-size:100%;
        background-repeat: no-repeat;
}

</style>

<div id="playex" class="player_sist <?php if(get_cookie('dt_player_width') == 'full') { echo 'fullplayer'; } ?>">
	<?php get_template_part('inc/parts/single/report-video'); ?>
	<?php  if ( $dt_player ) : ?>
	<div class="playex">
		<?php  if ($ads =='true') : ?>
		<div  id="playerads" class="ads_player">

			<div class="ads_box">
				<div class="ads">


					<?php if ($clic =='true'): ?><span class="notice"><?php _d('<img width="150" height="150" src="https://watchaly.com/wp-content/uploads/2018/06/Play-Button-PNG-Picture.png"/>'); ?></span><?php endif; ?>
          <?php if($ads_300) : echo '<div class="code">'. stripslashes($ads_300). '</div>'; endif; ?>

        </div>
			</div>
		</div>
		<?php endif; ?>




			<div id="option-<?php echo $numerado; ?>" class="play-box-iframe fixidtab">
              <img id="loader1" src="https://streamaly.me/curved-bars.svg" width="150" height="150" alt="loading gif"/>

				<iframe style="max-height:570px;" class="metaframe rptss" src="//streamaly.me/player.php?play=<?php echo $ids;

        foreach($uncompressed->genres as $genre){
          // echo "hello".$genre->id."<br>";
          if($genre->id === 16){


            echo '&type=anime&tmdb='.$tmdb.'&season='.$temp.'&episode='.$epis;
            $stop = "stop";
          }

        }
        if(empty($stop) && !empty($change_me)){


          echo "&type=tv&tmdb=$tmdb&season=$temp&episode=".$epis;
        }
        if(empty($stop) && !empty($change_me2)){


          echo '&type=movie&tmdb='.$uncompressed->id;

        }

        ?>" frameborder="0" scrolling="no" allowfullscreen></iframe>
			</div>



	</div>
	<?php  elseif ( !$dt_player ) : ?>
		<div class="playex">
		<?php  if ($ads =='true') : ?>
			<div id="playerads" class="ads_player">
				<div class="ads_box">
				<div class="ads">
					<?php if ($clic =='true'): ?><span class="notice">


          <?php _d('<img width="150" height="150" src="https://watchaly.com/wp-content/uploads/2018/06/Play-Button-PNG-Picture.png"/>'); ?></span><?php endif; ?>
          <?php if($ads_300) : echo '<div class="code"><div class="watchbla">'. stripslashes($ads_300). '</div></div>'; endif; ?>

      	</div>
				</div>
			</div>
		<?php endif; ?>
			<div id="option-streamaly" class="play-box-iframe fixidtab blacked">
             <img id="loader1" src="https://streamaly.me/curved-bars.svg" width="150" height="150" alt="loading gif"/>

				<iframe class="metaframe rptss" src="//streamaly.me/player.php?play=<?php echo $ids;
        foreach($uncompressed->genres as $genre){
          // echo "hello".$genre->id."<br>";
          if($genre->id === 16){


            echo '&type=anime&tmdb='.$tmdb.'&season='.$temp.'&episode='.$epis;
            $stop = "stop";
          }

        }
        if(empty($stop) && !empty($change_me)){


          echo "&type=tv&tmdb=$tmdb&season=$temp&episode=".$epis;
        }
        if(empty($stop) && !empty($change_me2)){


          echo '&type=movie&tmdb='.$uncompressed->id;

        }

                // echo $uncompressed->genres->id;

        ?>" frameborder="0" scrolling="no" allowfullscreen></iframe>
			</div>

		</div>
	<?php endif; ?>
	<div class="control">
		<nav class="player">
			<ul class="options">
				<li>
					<a class="sources"><i class="icon-menu listsormenu"></i> <b>SERVERS</b></a>
						<ul class="idTabs sourceslist">

							<li><a class="options" href="#option-streamaly">
							<b class="icon-play_arrow"></b> <?php echo 'PLayer 1'; ?>
							<span class="dt_flag"><img src="<?php echo DOO_URI, '/assets/img/flags/en.png'; ?>"></span>
							</a></li>
							<li><a class="options" href="#option-streamaly">
							<b class="icon-play_arrow"></b> <?php echo ' PLayer 2'; ?>
							<span class="dt_flag"><img src="<?php echo DOO_URI, '/assets/img/flags/en.png'; ?>"></span>
							</a></li>

						</ul>

				</li>
			</ul>

		</nav>
		<?php if ($qual =='true') : if($quali = $terms = strip_tags( $terms = get_the_term_list( $post->ID, 'dtquality'))) {  ?>
			<?php if($mostrar = $terms = strip_tags( $terms = get_the_term_list( $post->ID, 'dtquality'))) {  ?><span class="qualityx"><?php echo $mostrar; ?></span><?php } ?>
		<?php } endif; ?>

		<?php if ($viewsc =='true') : if($views) { $multiview = $views * 1000; echo '<span class="views"><strong>'. comvert_number($multiview ) .'</strong> '. __d('Views') .'</span>'; } endif; ?>

		<nav class="controles">

			<ul class="list">


				<?php  if ($ads =='true') : ?><li id="count" class="contadorads"><?php _d('Ad'); ?> <i id="contador"><?php echo $time; ?></i></li><?php endif; ?>
				<?php  if ($light =='true') : ?><li><a class="lightSwitcher" href="javascript:void(0);"><i class="icon-wb_sunny"></i></a></li><?php endif; ?>
				<?php  if ($report =='true') : if($reports=='10') { /* none*/ } else { ?><li><a class="report-video"><i class="icon-notification"></i> <span><?php _d('report'); ?></span></a></li><?php } endif; ?>
			<!---<li><a class="wide <?php// if(get_cookie('dt_player_width') == 'full') { echo 'reco'; } else { echo 'recox'; } ?>">!-->
			<!--	<i class="icons-enlarge2 <?php //if(get_cookie('dt_player_width') == 'full') { echo 'icons-shrink2'; } ?>"></i>!-->
				</a></li>
			</ul>

		</nav>
		</div>
</div>
<script type='text/javascript'>
	jQuery(document).ready(function($){
	$("#oscuridad").css("height", $(document).height()).hide();
	$(".lightSwitcher").click(function(){
	$("#oscuridad").toggle();
	if ($("#oscuridad").is(":hidden"))
	$(this).html("<i class='icon-wb_sunny'></i>").removeClass("turnedOff");
		else
	$(this).html("<i class='icon-wb_sunny'></i>").addClass("turnedOff");
		});
<?php  if ($ads =='true') : ?>
	var segundos = <?php echo $time; ?>;
	function ads_time(){
		var t = setTimeout( ads_time, 1000);
		document.getElementById('contador').innerHTML = '' +segundos--+'';
		if (segundos==0){
			$('#playerads').fadeOut('slow');
			$('#count').fadeOut('slow');
			clearInterval(setTimeout);
		}
	}
	ads_time();
<?php endif; ?>
<?php if ($clic =='true'): ?>
		$(".code").click(function() {
		  $("#playerads").fadeOut("slow");
		  $("#count").fadeOut("slow");
		});
		$(".notice").click(function() {
		  $("#playerads").fadeOut("slow");
		  $("#count").fadeOut("slow");
		});
<?php endif; ?>
	$(".options").click(function() {
	  $('.rptss').attr('src', function ( i, val ) { return val; });
	});
	});
</script>
<style>

@media only screen and (max-width: 500px) {
#ad3 {
height:30px;}
#notice{

margin-top:40px;

}



}


.circle {
stroke: #f8aa28;
stroke-dasharray: 650;
stroke-dashoffset: 650;
-webkit-transition: all 0.5s ease-in-out;
opacity: 0.3;
}

.playBut {
/*  border: 1px solid red;*/
display: inline-block;
-webkit-transition: all 0.5s ease;

.triangle {
-webkit-transition: all 0.7s ease-in-out;
stroke-dasharray: 240;
stroke-dashoffset: 480;
stroke: #000;
transform: translateY(0)
}


&:hover {

.triangle {
stroke-dashoffset: 0;
opacity: 1;
stroke: #f8aa28;
animation: nudge 0.7s ease-in-out;

@keyframes nudge{
0% {
transform: translateX(0)
}
30% {
transform: translateX(-5px)
}
50% {
transform: translateX(5px)
}
70% {
transform: translateX(-2px)
}
100% {
transform: translateX(0)
}
}
}

.circle {
stroke-dashoffset: 0;
opacity: 1;
}

}
}







</style>
