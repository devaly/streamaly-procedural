<?php get_header(); ?>
<!-- main -->
<div id="main">
<div class="container">
<?php $active = get_option('featslidmodule'); if ($active == "true") { ?>
<div class="top-content">
<!-- slider -->
<div id="slider" class="swiper-container-horizontal">
<?php get_template_part('includes/featured/slider'); ?>
<div class="swiper-pagination swiper-pagination-clickable"></div>
<div class="clearfix"></div>
</div>
<!--/slider -->
<!--top news-->
<?php get_template_part('includes/featured/news'); ?>
<!--/top news-->
<div class="clearfix"></div>
</div>
<?php get_template_part('includes/featured/social'); ?>
<?php }?>

<?php get_template_part('includes/aviso'); ?>

<div class="main-content">
<?php $active = get_option('homepage-ad-above'); if ($active == "true") { ?>

            <div class="content-kuss" style="">
<?php if ($note = get_option('ads-homepage-1-title')) { ?>
                <div id="content-kuss-title"> <span><?php echo $note; ?></span></div>
<?php }?>
				<?php if ($ads = get_option('ads-homepage-1-code')) { ?><div class="content-kuss-ads"><?php echo stripslashes($ads); ?></div><?php }?>
            </div>
<?php }?>


<?php $active = get_option('suggmodule'); if ($active == "true") { ?>
<?php include_once 'includes/home/suggestion/suggestion.php'; ?>
<?php }?>

<?php $active = get_option('homepage-ad-after'); if ($active == "true") { ?>

            <div class="content-kuss" style="">
<?php if ($note = get_option('ads-homepage-2-title')) { ?>
                <div id="content-kuss-title"> <span><?php echo $note; ?></span></div>
<?php }?>
				<?php if ($ads = get_option('ads-homepage-2-code')) { ?><div class="content-kuss-ads"><?php echo stripslashes($ads); ?></div><?php }?>
            </div>
<?php }?>

<?php $active = get_option('movsmodule'); if ($active == "true") { ?>
<!--latest movies-->
<div class="movies-list-wrap mlw-latestmovie">
<div class="ml-title">
<span class="pull-left"><?php if($title = get_option('latestmov_title')){ echo $title; } else { echo 'Latest Movies'; }?><i class="fa fa-chevron-right ml10"></i></span>
<a href="<?php echo get_option("mov_archive");?>" class="pull-right cat-more"><?php _e('View more »', 'psythemes'); ?></a>
<div class="clearfix"></div>
</div>
<div class="movies-list movies-list-full tab-pane in fade active">
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts( array('posts_per_page'=>$lmvnum,'post_type'=>array('post',),'paged'=>$paged ) );   ?>
<?php  if (have_posts()) : ?>
<?php post_movies_true(); ?>
<?php while (have_posts()) : the_post(); 
if(get_option('edd_sample_theme_license_key')) {
?>
<?php  if (has_post_thumbnail()) {
$imgsrc = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'medium');
$imgsrc = $imgsrc[0];
} elseif ($postimages = get_children("post_parent=$post->ID&post_type=attachment&post_mime_type=image&numberposts=0")) {
foreach($postimages as $postimage) {
$imgsrc = wp_get_attachment_image_src($postimage->ID, 'medium');
$imgsrc = $imgsrc[0];
}
} elseif (preg_match('/<img [^>]*src=["|\']([^"|\']+)/i', get_the_content(), $match) != FALSE) {
$imgsrc = $match[1];
} else {
if($img = get_post_custom_values($imagefix)){
$imgsrc = $img[0];
} else {
$img = get_template_directory_uri().'/images/noimg.png';
$imgsrc = $img;
} 
} ?>
<?php include 'includes/home/item.php'; ?>
<?php } endwhile; ?>						
<?php endif; ?>
<div class="clearfix"></div>
</div>
</div>
<?php }?>
 <!--/latest movies-->
 
<?php $active = get_option('tvmodule'); if ($active == "true") { ?>
<?php include_once 'home-tvshows.php'; ?>
<?php } ?>			
<?php $active = get_option('epmodule'); if ($active == "true") { ?>
<?php include_once 'home-episodes.php'; ?>
<?php } ?>			


<?php $active = get_option('homepage-ad-before'); if ($active == "true") { ?>

            <div class="content-kuss" style="">
<?php if ($note = get_option('ads-homepage-3-title')) { ?>
                <div id="content-kuss-title"> <span><?php echo $note; ?></span></div>
<?php }?>
				<?php if ($ads = get_option('ads-homepage-3-code')) { ?><div class="content-kuss-ads"><?php echo stripslashes($ads); ?></div><?php }?>
            </div>
<?php }?>	
</div>
</div>
</div>
<!--/main -->
<?php  get_footer(); ?>